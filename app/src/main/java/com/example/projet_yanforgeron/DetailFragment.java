package com.example.projet_yanforgeron;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView artifactDetailName, artifactDetailDescription, artifactDetailYear, artifactDetailBrand, artifactTechnicalDetails, artifactDetailCategories, artifactDetailTimeFrame;
    private ImageView artifactDetailImage;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private PictureAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        //Get selected artifact
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String artifactId = args.getArtifactId();
        viewModel.setArtifact(artifactId);
        listenerSetup();
        observerSetup();

    }

    private void listenerSetup(){
        artifactDetailBrand = getView().findViewById(R.id.artifact_detail_brand);
        artifactDetailName = getView().findViewById(R.id.artifact_detail_name);
        artifactDetailDescription = getView().findViewById(R.id.artifact_detail_description);
        artifactDetailYear = getView().findViewById(R.id.artifact_detail_year);
        artifactDetailImage = getView().findViewById(R.id.artifact_detail_image);
        artifactTechnicalDetails = getView().findViewById(R.id.artifact_detail_technicalDetails);
        artifactDetailCategories = getView().findViewById(R.id.artifact_detail_categories);
        artifactDetailTimeFrame = getView().findViewById(R.id.artifact_detail_timeFrame);

        adapter = new PictureAdapter();
        recyclerView = getView().findViewById(R.id.picture_recycler_view);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);

        getView().findViewById(R.id.button_previous).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup(){
        viewModel.getArtifact().observe(getViewLifecycleOwner(),
                artifact -> {
                    if(artifact != null){
                        adapter.setCurrentArtifact(artifact);
                        adapter.setPictures(artifact.getPictures());
                        artifactDetailBrand.setText(artifact.getBrand());
                        artifactDetailDescription.setText(artifact.getDescription());
                        artifactDetailName.setText(artifact.getName());
                        if(artifact.getTechnicalDetails() != null){
                            String[] list = artifact.getTechnicalDetails().split("#");
                            StringBuilder string = new StringBuilder();
                            for (String s : list){
                                string.append(s).append(" - ");
                            }
                            string.delete(string.length()-3, string.length());
                            String toShow = getString(R.string.technicalDetailsString) + string.toString();
                            artifactTechnicalDetails.setText(toShow);
                        }
                        if(artifact.getTimeFrame() != null){
                            String toShow = getString(R.string.timeFrameString) + artifact.getTimeFrame();
                            artifactDetailTimeFrame.setText(toShow);
                        }

                        String[] list = artifact.getCategories().split("#");
                        StringBuilder string = new StringBuilder();
                        for (String s : list){
                            string.append(s).append(" - ");
                        }
                        string.delete(string.length()-3, string.length());
                        String toShow = getString(R.string.detailCategoriesString) + string.toString();
                        artifactDetailCategories.setText(toShow);


                        if(artifact.getYear() != null) artifactDetailYear.setText(artifact.getYear().toString());
                        Glide.with(artifactDetailImage.getContext())
                                .load("https://demo-lia.univ-avignon.fr/cerimuseum/items/" + artifact.getId() + "/thumbnail")
                                .into(artifactDetailImage);
                    }
                }
                );
    }
}
