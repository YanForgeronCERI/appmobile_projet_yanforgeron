package com.example.projet_yanforgeron;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.projet_yanforgeron.data.artifact.Artifact;
import com.example.projet_yanforgeron.data.artifact.ArtifactRepository;

import java.util.List;

public class ListViewModel extends AndroidViewModel {

    private ArtifactRepository repository;
    private LiveData<List<Artifact>> allArtifacts;
    private LiveData<List<Artifact>> allArtifactsOrderedByYear;


    public ListViewModel(@NonNull Application application) {
        super(application);
        repository = ArtifactRepository.get(application);
        allArtifacts = repository.getAllArtifacts();
        allArtifactsOrderedByYear = repository.getAllArtifactsOrderedByYear();
    }

    LiveData<List<Artifact>> getAllArtifacts() {
        return allArtifacts;
    }

    LiveData<List<Artifact>> getAllArtifactsOrderedByYear() {
        return allArtifactsOrderedByYear;
    }

}
