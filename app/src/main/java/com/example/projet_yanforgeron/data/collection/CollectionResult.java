package com.example.projet_yanforgeron.data.collection;

import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.LiveData;

import com.example.projet_yanforgeron.data.artifact.Artifact;
import com.example.projet_yanforgeron.data.artifact.ArtifactResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CollectionResult {
    public static void transferInfo(Map<String, ArtifactResponse> map, List<Artifact> artifacts){
        for (Map.Entry<String, ArtifactResponse> entry: map.entrySet()){
            Artifact artifact = new Artifact(entry.getKey(), entry.getValue().name);
            artifact.setDescription(entry.getValue().description);
            artifact.setTechnicalDetails(entry.getValue().technicalDetails);
            artifact.setCategories(entry.getValue().categories);
            artifact.setPictures(entry.getValue().pictures);
            artifact.setWorking(entry.getValue().working);
            artifact.setYear(entry.getValue().year);
            artifact.setBrand(entry.getValue().brand);
            artifact.setTimeFrame(entry.getValue().timeFrame);
            artifacts.add(artifact);
        }

    }
}
