package com.example.projet_yanforgeron.data.artifact;

import java.util.List;
import java.util.Map;

public class ArtifactResponse {
    public final String description = null;
    public final List<String> technicalDetails = null;
    public final List<String> categories = null;
    public final String name = null;
    public final Map<String, String> pictures = null;
    public final Boolean working = null;
    public final Integer year = null;
    public final String brand = null;
    public final List<Integer> timeFrame = null;
}
