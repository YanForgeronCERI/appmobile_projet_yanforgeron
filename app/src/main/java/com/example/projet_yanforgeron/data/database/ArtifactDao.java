package com.example.projet_yanforgeron.data.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.projet_yanforgeron.data.artifact.Artifact;

import java.util.List;

@Dao
public interface ArtifactDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Artifact artifact);

    @Query("SELECT * FROM artifact_table ORDER BY name")
    LiveData<List<Artifact>> getAllArtifacts();
;
    @Query("SELECT * FROM artifact_table ORDER BY year DESC")
    LiveData<List<Artifact>> getAllArtifactsOrderByYear();

    @Query("SELECT * FROM artifact_table WHERE id = :id")
    Artifact getArtifactById(String id);

}
