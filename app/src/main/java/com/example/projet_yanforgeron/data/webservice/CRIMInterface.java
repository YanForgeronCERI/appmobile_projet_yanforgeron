package com.example.projet_yanforgeron.data.webservice;

import com.example.projet_yanforgeron.data.artifact.ArtifactResponse;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface CRIMInterface {
    @Headers("Accept: application/geo+json")
    @GET("collection")
    Call<Map<String, ArtifactResponse>> getCollection();

}
