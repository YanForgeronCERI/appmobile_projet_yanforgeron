package com.example.projet_yanforgeron.data.database;

import android.content.Context;
import android.util.Log;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.projet_yanforgeron.data.artifact.Artifact;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Artifact.class}, version = 1, exportSchema = false)
public abstract class ArtifactRoomDatabase extends RoomDatabase {

    private static final String TAG = ArtifactRoomDatabase.class.getSimpleName();

    public abstract ArtifactDao artifactDao();

    private static ArtifactRoomDatabase INSTANCE;
    private static  final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static ArtifactRoomDatabase getDatabase(final Context context){
        if (INSTANCE == null) {
            synchronized (ArtifactRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    // without populate

                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    ArtifactRoomDatabase.class,"artifact_database")
                                    .build();
                }
            }
        }
        return INSTANCE;
    }

}
