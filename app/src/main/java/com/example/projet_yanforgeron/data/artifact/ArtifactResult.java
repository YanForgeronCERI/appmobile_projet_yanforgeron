package com.example.projet_yanforgeron.data.artifact;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class ArtifactResult {
    public static void transferInfo(ArtifactResponse artifactResponse, Artifact artifact){
        artifact.setDescription(artifactResponse.description);
        artifact.setTechnicalDetails(artifactResponse.technicalDetails);
        artifact.setCategories(artifactResponse.categories);
        artifact.setName(artifactResponse.name);
        artifact.setPictures(artifactResponse.pictures);
        artifact.setWorking(artifactResponse.working);
        artifact.setYear(artifactResponse.year);
        artifact.setBrand(artifactResponse.brand);
    }
}
