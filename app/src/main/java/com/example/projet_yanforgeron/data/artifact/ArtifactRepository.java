package com.example.projet_yanforgeron.data.artifact;

import android.app.Application;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.projet_yanforgeron.data.collection.CollectionResult;
import com.example.projet_yanforgeron.data.database.ArtifactDao;
import com.example.projet_yanforgeron.data.database.ArtifactRoomDatabase;
import com.example.projet_yanforgeron.data.webservice.CRIMInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static com.example.projet_yanforgeron.data.database.ArtifactRoomDatabase.databaseWriteExecutor;

public class ArtifactRepository {

    private static final String TAG = ArtifactRepository.class.getSimpleName();

    private LiveData<List<Artifact>> allArtifacts;
    private LiveData<List<Artifact>> allArtifactsOrderedByYear;
    private MutableLiveData<Artifact> selectedArtifact;
    private final CRIMInterface api;
    private ArtifactDao artifactDao;

    public static volatile  ArtifactRepository INSTANCE;

    public synchronized static ArtifactRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new ArtifactRepository(application);
        }

        return INSTANCE;
    }



    public ArtifactRepository(Application application){
        ArtifactRoomDatabase db = ArtifactRoomDatabase.getDatabase(application);
        artifactDao = db.artifactDao();
        allArtifacts = artifactDao.getAllArtifacts();
        allArtifactsOrderedByYear = artifactDao.getAllArtifactsOrderByYear();

        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();
        api = retrofit.create(CRIMInterface.class);
        if(allArtifacts.getValue() == null || allArtifacts.getValue().size() == 0){
            loadAllArtifacts();
            allArtifacts = artifactDao.getAllArtifacts();
        }
        selectedArtifact = new MutableLiveData<>();
    }


    public LiveData<List<Artifact>> getAllArtifacts() {
        return allArtifacts;
    }

    public LiveData<List<Artifact>> getAllArtifactsOrderedByYear() {
        return allArtifactsOrderedByYear;
    }

    public MutableLiveData<Artifact> getSelectedArtifact() {
        return selectedArtifact;
    }

    public long insertArtifact(Artifact artifact) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return artifactDao.insert(artifact);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedArtifact.setValue(artifact);
        return res;
    }

    public void getArtifact(String id){
        Future<Artifact> fArtifact = databaseWriteExecutor.submit(() -> {
            return artifactDao.getArtifactById(id);
        });
        try {
            selectedArtifact.setValue(fArtifact.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



    public void loadAllArtifacts(){

        api.getCollection().enqueue(
            new Callback<Map<String, ArtifactResponse>>() {
                @Override
                public void onResponse(Call<Map<String, ArtifactResponse>> call, Response<Map<String, ArtifactResponse>> response) {
                    List<Artifact> artifacts = new ArrayList<Artifact>();
                    if(response.body() != null){
                        CollectionResult.transferInfo(response.body(), artifacts);
                        for(Artifact a : artifacts){
                            insertArtifact(a);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Map<String, ArtifactResponse>> call, Throwable t) {

                }
            }
        );
    }

}
