package com.example.projet_yanforgeron;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.projet_yanforgeron.data.artifact.Artifact;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<com.example.projet_yanforgeron.RecyclerAdapter.ViewHolder> {


    private List<Artifact> artifactList;
    private List<Artifact> defaultArtifactList;
    private SearchView searchView;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if(viewType == 0){
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.left_artifact_layout, parent, false);
        }else{
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.right_artifact_layout, parent, false);
        }
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.artifactName.setText(artifactList.get(position).getName());
        holder.artifactBrand.setText(artifactList.get(position).getBrand());
        if(artifactList.get(position).getYear() != null){
            holder.artifactYear.setText(artifactList.get(position).getYear().toString());
        }else{
            holder.artifactYear.setText("");
        }

        String[] list = artifactList.get(position).getCategories().split("#");
        StringBuilder string = new StringBuilder();
        for (String s : list){
            string.append(s).append(" - ");
        }
        string.delete(string.length()-3, string.length());
        holder.artifactCategories.setText(string.toString());
        Glide.with(holder.artifactImage.getContext())
                .load("https://demo-lia.univ-avignon.fr/cerimuseum/items/" + artifactList.get(position).getId() + "/thumbnail")
                .into(holder.artifactImage);

    }

    @Override
    public int getItemViewType(int position) {
        return position % 2;
    }

    @Override
    public int getItemCount() {
        if(artifactList != null){
            return getSearchedArtifacts().size();
        }
        return 0;
    }

    public List<Artifact> getSearchedArtifacts(){
        if(defaultArtifactList == null) return new ArrayList<Artifact>();
        CharSequence searchValue = searchView.getQuery();
        List<Artifact> pArtifacts = new ArrayList<>();
        if(searchValue.length() != 0){
            for(Artifact artifact: defaultArtifactList){

                if(artifact.getName().toLowerCase().contains(searchValue.toString().toLowerCase())){
                    if(artifact.getYear() != null){

                        Log.e(null, artifact.getName() + " " + artifact.getYear().toString());
                    }else{
                        Log.e(null, artifact.getName());
                    }
                    pArtifacts.add(artifact);
                }
            }
        }else{
            pArtifacts = defaultArtifactList;
        }
        return  pArtifacts;
    }

    public void setDefaultArtifactList(List<Artifact> defaultArtifactList){
        this.defaultArtifactList = defaultArtifactList;
    }

    public void setSearchView(SearchView searchView){
        this.searchView = searchView;

        this.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                setArtifactList(artifactList);
                return false;
            }
        });
    }



    public void setArtifactList(List<Artifact> artifacts) {
        artifactList = getSearchedArtifacts();
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView artifactName;
        TextView artifactBrand;
        TextView artifactCategories;
        TextView artifactYear;
        ImageView artifactImage;


        ViewHolder(View itemView) {
            super(itemView);
            artifactName = itemView.findViewById(R.id.artifact_name);
            artifactBrand = itemView.findViewById(R.id.artifact_brand);
            artifactCategories = itemView.findViewById(R.id.artifact_categories);
            artifactImage = itemView.findViewById(R.id.artifact_image);
            artifactYear = itemView.findViewById(R.id.artifact_year);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    String id = RecyclerAdapter.this.artifactList.get((int)getAdapterPosition()).getId();

                    ListFragmentDirections.ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment(id);
                    Navigation.findNavController(v).navigate(action);

                }
            });

        }




    }
}
