package com.example.projet_yanforgeron;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.projet_yanforgeron.R;
import com.example.projet_yanforgeron.data.artifact.Artifact;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class PictureAdapter extends RecyclerView.Adapter<PictureAdapter.ViewHolder> {

    private LinkedHashMap<String, String> pictures;
    private Artifact currentArtifact;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.picture_card, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.pictureDescription.setText(new ArrayList<String>(pictures.values()).get(position));
        Glide.with(holder.pictureImage.getContext())
                .load("https://demo-lia.univ-avignon.fr/cerimuseum/items/" + currentArtifact.getId() + "/images/" + new ArrayList<String>(pictures.keySet()).get(position))
                .into(holder.pictureImage);
    }

    @Override
    public int getItemCount() {
        if(pictures != null){
            return pictures.size();
        }
        return 0;
    }

    public void setPictures(String stringPicture){
        if(stringPicture == null) return;
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        String substring = stringPicture.substring(1, stringPicture.length()-1);
        String[] pairs = substring.split("#");
        for(int i = 0; i < pairs.length; i++){
            String pair = pairs[i];
            String[] keyValue = pair.split("=");
            String description = "";
            if(keyValue.length > 1 ){
                description = keyValue[1];
            }
            map.put(keyValue[0], description);
        }
        pictures = map;
    }

    public void setCurrentArtifact(Artifact artifact){
        currentArtifact = artifact;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView pictureDescription;
        ImageView pictureImage;


        ViewHolder(View itemView) {
            super(itemView);
            pictureDescription = itemView.findViewById(R.id.picture_description);
            pictureImage = itemView.findViewById(R.id.picture_image);

        }




    }
}
