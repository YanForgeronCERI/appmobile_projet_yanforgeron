package com.example.projet_yanforgeron;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.projet_yanforgeron.data.artifact.Artifact;
import com.example.projet_yanforgeron.data.artifact.ArtifactRepository;

import java.util.ArrayList;
import java.util.List;

public class DetailViewModel  extends AndroidViewModel {

    private ArtifactRepository repository;
    private MutableLiveData<Artifact> artifact;

    public DetailViewModel (Application application){
        super(application);
        repository = ArtifactRepository.get(application);
        artifact = new MutableLiveData<>();
    }

    public void setArtifact(String id){
        repository.getArtifact(id);
        artifact = repository.getSelectedArtifact();
    }



    public LiveData<Artifact> getArtifact(){
        return artifact;
    }

}
