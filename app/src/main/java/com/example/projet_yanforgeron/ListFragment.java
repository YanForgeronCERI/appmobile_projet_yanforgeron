package com.example.projet_yanforgeron;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.Collections;

public class ListFragment extends Fragment {


    private ListViewModel viewModel;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerAdapter recyclerAdapter;
    private Spinner spinner;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(ListViewModel.class);
        listenerSetup();
        observerSetup();

    }

    private void listenerSetup() {
        recyclerView = getView().findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerAdapter = new RecyclerAdapter();
        recyclerView.setAdapter(recyclerAdapter);
        recyclerAdapter.setSearchView(getView().findViewById(R.id.searchView));
        spinner = getView().findViewById(R.id.spinner);
        String[] choices = { "Abc", "Date"};
        ArrayAdapter<String> aa = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, choices);
        aa.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(aa);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                observerSetup();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


    private void observerSetup() {

        String choice = spinner.getSelectedItem().toString();
        if(choice.equals("Abc")){
            viewModel.getAllArtifacts().observe(getViewLifecycleOwner(),
                    artifacts -> {
                        recyclerAdapter.setDefaultArtifactList(artifacts);
                        recyclerAdapter.setArtifactList(artifacts);
                    });
        }else{
            viewModel.getAllArtifactsOrderedByYear().observe(getViewLifecycleOwner(),
                    artifacts -> {
                        recyclerAdapter.setDefaultArtifactList(artifacts);
                        recyclerAdapter.setArtifactList(artifacts);
                    });
        }
    }

}
